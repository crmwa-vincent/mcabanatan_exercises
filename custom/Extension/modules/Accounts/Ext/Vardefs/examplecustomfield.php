<?php

$dictionary['account']['fields']['example_c']['name'] = 'example_c';
$dictionary['account']['fields']['example_c']['vname'] = 'LBL_EXAMPLE_C';
$dictionary['account']['fields']['example_c']['type'] = 'varchar';
$dictionary['account']['fields']['example_c']['enforced'] = '';
$dictionary['account']['fields']['example_c']['dependency'] = '';
$dictionary['account']['fields']['example_c']['required'] = false;
$dictionary['account']['fields']['example_c']['massupdate'] = '0';
$dictionary['account']['fields']['example_c']['default'] = '';
$dictionary['account']['fields']['example_c']['no_default'] = false;
$dictionary['account']['fields']['example_c']['comments'] = 'Example Varchar Vardef';
$dictionary['account']['fields']['example_c']['help'] = '';
$dictionary['account']['fields']['example_c']['importable'] = 'true';
$dictionary['account']['fields']['example_c']['duplicate_merge'] = 'disabled';
$dictionary['account']['fields']['example_c']['duplicate_merge_dom_value'] = '0';
$dictionary['account']['fields']['example_c']['audited'] = false;
$dictionary['account']['fields']['example_c']['reportable'] = true;
$dictionary['account']['fields']['example_c']['unified_search'] = false;
$dictionary['account']['fields']['example_c']['merge_filter'] = 'disabled';
$dictionary['account']['fields']['example_c']['calculated'] = false;
$dictionary['account']['fields']['example_c']['len'] = '255';
$dictionary['account']['fields']['example_c']['size'] = '20';
$dictionary['account']['fields']['example_c']['id'] = 'example_c';
$dictionary['account']['fields']['example_c']['custom_module'] = '';

//required to create the field in the _cstm table
$dictionary['account']['fields']['example_c']['source'] = 'cust0m_fields';