<?php
/**
 * Created by PhpStorm.
 * User: Marjhun
 * Date: 6/16/2015
 * Time: 2:09 PM
 */

$viewdefs['accounts']['base']['menu']['header'][] = array(
    'route'=>'#Leads/',
    'label' =>'LNK_LEADS_C',
    'acl_module'=>'Leads',
    'icon' => 'icon-user',
);