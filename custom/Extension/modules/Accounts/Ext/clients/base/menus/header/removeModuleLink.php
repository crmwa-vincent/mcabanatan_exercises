<?php
/**
 * Created by PhpStorm.
 * User: Marjhun
 * Date: 6/16/2015
 * Time: 2:29 PM
 */

if (isset($viewdefs['Accounts']['base']['menu']['header']))
{
    foreach ($viewdefs['Accounts']['base']['menu']['header'] as $key => $moduleAction)
    {
        //remove the link by label key
        if ($moduleAction['label'] == "LNK_LEADS_C")
        {
            unset($viewdefs['Accounts']['base']['menu']['header'][$key]);
        }
    }
}