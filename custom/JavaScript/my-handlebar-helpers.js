/**
 * Created by Marjhun on 6/10/2015.
 */
/**
 * Handlebars helpers.
 *
 * These functions are to be used in handlebars templates.
 * @class Handlebars.helpers
 * @singleton
 */
(function(app) {
    app.events.on("app:init", function() {

        /**
         * convert a string to upper case
         */
        Handlebars.registerHelper("customUpperCase", function (text)
        {
            return text.toUpperCase();
        });

        /**
         * convert a string to lower case
         */
        Handlebars.registerHelper("customLowerCase", function (text)
        {
            return text.toLowerCase();
        });

    });
})(SUGAR.App);