<?php
/**
 * Created by PhpStorm.
 * User: Marjhun
 * Date: 6/22/2015
 * Time: 10:31 AM
 */

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$connector_strings = array (
    //Vardef labels
    'LBL_LICENSING_INFO' => '<table border="0" cellspacing="1"><tr><td valign="top" width="35%" class="dataLabel">Licensing Info</td></tr></table>',
    'LBL_FULL_NAME' => 'Full Name',
    'LBL_FIRST_NAME' => 'First Name',
    'LBL_LAST_NAME' => 'Last Name',
    'LBL_EMAIL' => 'Email Address',
    'LBL_STATE' => 'State',

    //Configuration labels
    'max_results' => 'Max Results',
);

