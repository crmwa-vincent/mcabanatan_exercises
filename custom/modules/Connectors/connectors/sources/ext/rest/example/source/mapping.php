<?php
/**
 * Created by PhpStorm.
 * User: Marjhun
 * Date: 6/22/2015
 * Time: 10:34 AM
 */


$mapping = array (
    'beans' =>
        array (
            'Contacts' =>
                array (
                    'firstname' => 'first_name',
                    'lastname' => 'last_name',
                    'email' => 'email1',
                    'state' => 'primary_address_state',
                    'fullname' => 'full_name',
                    'id' => 'id',
                ),
        ),
);