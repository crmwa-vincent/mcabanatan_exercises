<?php
/**
 * Created by PhpStorm.
 * User: Marjhun
 * Date: 6/22/2015
 * Time: 10:11 AM
 */

/**
 *CHECK ENTRY POINT
 */
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/**
 *REQUIRE CONNECTORS REST API FOR EXTENDING
 */
require_once('include/connectors/sources/ext/rest/rest.php');

/**
 *EXTEND exe_rest_example to ext_rest
 */
class ext_rest_example extends ext_rest
{
    public function __construct()
    {
        parent::__construct();

        //Used for testing
        $this->_has_testing_enabled = true;

        //Used to enable hover for the formatter
        $this->_enable_in_hover = true;
    }

    /**
     * test
     * Returns true or false to test criteria
     *
     * @return Boolean true if test completes successfully
     */
    public function test()
    {
        $item = $this->getItem(array('id'=>'1'));
        return !empty($item['firstname']) && ($item['firstname'] == 'John');
    }

    /**
     * getItem
     * Returns an array containing a key/value pair(s) of a source record
     *
     * @param Array $args Array of arguments to search/filter by
     * @param String $module String optional value of the module that the connector framework is attempting to map to
     * @return Array of key/value pair(s) of the source record; empty Array if no results are found
     */
    public function getItem($args=array(), $module=null)
    {
        $result = null;
        if ($args['id'] == 1)
            {
            $result = array();
            $result['id'] = '1'; //Unique record identifier
            $result['firstname'] = 'John';
            $result['lastname'] = 'Doe';
            $result['email'] = 'john.doe@sugar.crm';
            $result['state'] = 'CA';
        }
        else if ($args['id'] == 2)
        {
            $result = array();
            $result['id'] = '2'; //Unique record identifier
            $result['firstname'] = 'Jane';
            $result['lastname'] = 'Doe';
            $result['email'] = 'jane.doe@sugar.crm';
            $result['state'] = 'HI';
        }

        return $result;
    }

    /**
     * getList
     * Returns a nested array containing a key/value pair(s) of a source record
     *
     * @param Array $args Array of arguments to search/filter by
     * @param String $module String optional value of the module that the connector framework is attempting to map to
     * @return Array of key/value pair(s) of source record; empty Array if no results are found
     */
    public function getList($args=array(), $module=null)
    {
        $results = array();
        if(!empty($args['name']['last']) && strtolower($args['name']['last']) == 'doe')
        {
            $results[1] = array('
                    id' => 1,
                'firstname' => 'John',
                'lastname' => 'Doe',
                'email' => 'john.doe@sugar.crm',
                'state' => 'CA'
            );

            $results[2] = array(
                'id' => 2,
                'firstname' => 'Jane',
                'lastname' => 'Doe',
                'email' => 'john.doe@sugar.crm',
                'state' => 'HI'
            );
        }

        return $results;
    }
}

?>