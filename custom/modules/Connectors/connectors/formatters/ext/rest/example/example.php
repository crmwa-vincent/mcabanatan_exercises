<?php
/**
 * Created by PhpStorm.
 * User: Marjhun
 * Date: 6/22/2015
 * Time: 10:36 AM
 */


if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/connectors/formatters/default/formatter.php');

class ext_rest_example_formatter extends default_formatter
{

    public function getDetailViewFormat()
    {
        $mapping = $this->getSourceMapping();
        $mapping_name = !empty($mapping['beans'][$this->_module]['fullname']) ? $mapping['beans'][$this->_module]['fullname'] : '';

        if(!empty($mapping_name))
        {
            $this->_ss->assign('mapping_name', $mapping_name);
            return $this->fetchSmarty();
        }

        $GLOBALS['log']->error($GLOBALS['app_strings']['ERR_MISSING_MAPPING_ENTRY_FORM_MODULE']);
        return '';
    }

    public function getIconFilePath()
    {
        //icon for display
        return 'themes/Sugar/images/icon_basic_32.png';
    }

}