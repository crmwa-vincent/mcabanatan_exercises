/**
 * Created by Marjhun on 6/10/2015.
 */
({

    className: 'tcenter',

    loadData: function (options) {
        //populate your data
        myData=new Object();
        myData.myProperty = "My Value";

        this.myData = myData;

        /*
         //alternatively, you can pass in a JSON array to populate your data
         myData = $.parseJSON( '{"myData":{"myProperty":"My Value"}}' );
         _.extend(this, myData);
         */

        this.render();
    }

})