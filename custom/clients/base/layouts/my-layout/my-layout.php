<?php
/**
 * Created by PhpStorm.
 * User: Marjhun
 * Date: 6/8/2015
 * Time: 4:09 PM
 */
$viewdefs['base']['layout']['my-layout'] = array(
    'type' => 'simple',
    'components' => array(
        array(
            'view' => 'my-view',
        ),
    ),
);
