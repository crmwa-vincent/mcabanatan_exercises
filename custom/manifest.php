<?php

    $manifest =array(
        'acceptable_sugar_flavors' => array('CE','PRO','CORP','ENT','ULT'),
        'acceptable_sugar_versions' => array(
            'exact_matches' => array(),
	        'regex_matches' => array(
	            0 => '6\\.5\\.(.*?)',
	            1 => '6\\.7\\.(.*?)',
	            2 => '7\\.2\\.(.*?)', 
	            3 => '7\\.2\\.(.*?)\\.(.*?)', 
	            4 => '7\\.5\\.(.*?)\\.(.*?)', 
	            5 => '7\\.6\\.(.*?)\\.(.*?)'
	        ),
        ),
        'author' => 'SugarCRM',
        'description' => 'Installs a sample set of custom fields to the accounts module',
        'icon' => '',
        'is_uninstallable' => true,
        'name' => 'Example Custom Field Installer',
        'published_date' => '2012-07-06 2012 20:45:04',
        'type' => 'module',
        'version' => '1391607505',
    );
    
    $installdefs =array(
        'id' => 'package_1341607504',
        'language'=> array (
			array(
				'from'=> '<basepath>/Files/Language/Accounts/en_us.lang.php',
				'to_module'=> 'Accounts',
				'language'=>'en_us'
			),
		),
		'custom_fields'=>array (
		    //Text
		    array(
		        'name' => 'text_field_example_c',
		        'label' => 'LBL_TEXT_FIELD_EXAMPLE',
		        'type' => 'varchar',
		        'module' => 'Accounts',
		        'help' => 'Text Field Help Text',
		        'comment' => 'Text Field Comment Text',
		        'default_value' => '',
		        'max_size' => 255,
		        'required' => false, // true or false
		        'reportable' => true, // true or false
		        'audited' => false, // true or false
		        'importable' => 'true', // 'true', 'false', 'required'
		        'duplicate_merge' => false, // true or false
		    ),
		    //DropDown
		    array(
		        'name' => 'dropdown_field_example_c',
		        'label' => 'LBL_DROPDOWN_FIELD_EXAMPLE',
		        'type' => 'enum',
		        'module' => 'Accounts',
		        'help' => 'Enum Field Help Text',
		        'comment' => 'Enum Field Comment Text',
		        'ext1' => 'account_type_dom', //maps to options - specify list name
		        'default_value' => 'Analyst', //key of entry in specified list
		        'mass_update' => false, // true or false
		        'required' => false, // true or false
		        'reportable' => true, // true or false
		        'audited' => false, // true or false
		        'importable' => 'true', // 'true', 'false' or 'required'
		        'duplicate_merge' => false, // true or false
		    ),
		    //MultiSelect
		    array(
		        'name' => 'multiselect_field_example_c',
		        'label' => 'LBL_MULTISELECT_FIELD_EXAMPLE',
		        'type' => 'multienum',
		        'module' => 'Accounts',
		        'help' => 'Multi-Enum Field Help Text',
		        'comment' => 'Multi-Enum Field Comment Text',
		        'ext1' => 'account_type_dom', //maps to options - specify list name
		        'default_value' => 'Analyst', //key of entry in specified list
		        'mass_update' => false, // true or false
		        'required' => false, // true or false
		        'reportable' => true, // true or false
		        'audited' => false, // true or false
		        'importable' => 'true', // 'true', 'false' or 'required'
		        'duplicate_merge' => false, // true or false
		    ),
		    //Checkbox
		    array(
		        'name' => 'checkbox_field_example_c',
		        'label' => 'LBL_CHECKBOX_FIELD_EXAMPLE',
		        'type' => 'bool',
		        'module' => 'Accounts',
		        'default_value' => true, // true or false
		        'help' => 'Bool Field Help Text',
		        'comment' => 'Bool Field Comment',
		        'audited' => false, // true or false
		        'mass_update' => false, // true or false
		        'duplicate_merge' => false, // true or false
		        'reportable' => true, // true or false
		        'importable' => 'true', // 'true', 'false' or 'required'
		    ),
		    //Date
		    array(
		        'name' => 'date_field_example_c',
		        'label' => 'LBL_DATE_FIELD_EXAMPLE',
		        'type' => 'date',
		        'module' => 'Accounts',
		        'default_value' => '',
		        'help' => 'Date Field Help Text',
		        'comment' => 'Date Field Comment',
		        'mass_update' => false, // true or false
		        'required' => false, // true or false
		        'reportable' => true, // true or false
		        'audited' => false, // true or false
		        'duplicate_merge' => false, // true or false
		        'importable' => 'true', // 'true', 'false' or 'required'
		    ),
		    //DateTime
		    array(
		        'name' => 'datetime_field_example_c',
		        'label' => 'LBL_DATETIME_FIELD_EXAMPLE',
		        'type' => 'datetime',
		        'module' => 'Accounts',
		        'default_value' => '',
		        'help' => 'DateTime Field Help Text',
		        'comment' => 'DateTime Field Comment',
		        'mass_update' => false, // true or false
		        'enable_range_search' => false, // true or false
		        'required' => false, // true or false
		        'reportable' => true, // true or false
		        'audited' => false, // true or false
		        'duplicate_merge' => false, // true or false
		        'importable' => 'true', // 'true', 'false' or 'required'
		    ),
		    //Encrypt
		    array(
		        'name' => 'encrypt_field_example_c',
		        'label' => 'LBL_ENCRYPT_FIELD_EXAMPLE',
		        'type' => 'encrypt',
		        'module' => 'Accounts',
		        'default_value' => '',
		        'help' => 'Encrypt Field Help Text',
		        'comment' => 'Encrypt Field Comment',
		        'reportable' => true, // true or false
		        'audited' => false, // true or false
		        'duplicate_merge' => false, // true or false
		        'importable' => 'true', // 'true', 'false' or 'required'
		    ),
		),
    );

?>